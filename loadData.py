#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 11:12:36 2016

@author: qiaonan
"""

import pickle
import numpy as np
import qn
import os
import qn

def getSaveDir(path):
    filename = qn.getfilename(path)
    tag = filename.split('.')[0]
    targetDir = 'models/'+tag
    if os.path.isdir(targetDir):
        os.rmdir(targetDir)
    os.mkdir(targetDir)
    return targetDir

def loadCustom(path,dataSplit=[6,1,3]):
    filename = qn.getfilename(path)
    splits = filename.split('-')
    sentMax = int(splits[1])
    wordMax = int(splits[3])
    with open(path,'rb') as pf:
        data = pickle.load(pf)
        labels = pickle.load(pf)
        vocabSize = pickle.load(pf)

    count = len(data)
    testCount = int(count/10*dataSplit[2])
    validCount = int(count/10*dataSplit[1])
    trainCount = count - testCount - validCount

    def getByRange(rng):
        return [data[i] for i in rng], [labels[i] for i in rng]

    trainX, trainY = getByRange(range(trainCount))
    validX, validY = getByRange(range(trainCount,trainCount+validCount))
    testX, testY = getByRange(range(trainCount+validCount,count))

    return trainX, trainY, validX, validY, testX, testY, sentMax, wordMax, vocabSize


def loadData(maxlen,vocabSize):
    with open('imdb.pkl','rb') as mf:
        trainSet = pickle.load(mf)
        testSet = pickle.load(mf)

    def filterSet(dataset,maxlen,shuffle=False):
        filtered = list(filter(lambda x:len(x[0])<maxlen,zip(dataset[0],dataset[1])))
        if shuffle:
            np.random.shuffle(filtered)
        filtered = list(zip(*filtered))
        return [list(filtered[0]),list(filtered[1])]

    trainSetF = filterSet(trainSet,maxlen)
    testSetF = filterSet(testSet,maxlen,True)

    def formularize(dataset,maxlen,vocabSize):
        def formularize(item):
            # vocab restriction
            item = [x if x<vocabSize else 1 for x in item]
            # padding
            return item + [0]*(maxlen-len(item))
        return [formularize(item) for item in dataset]

    trainX = formularize(trainSetF[0],maxlen,vocabSize)
    testX = formularize(testSetF[0],maxlen,vocabSize)

    return trainX, trainSetF[1], testX, testSetF[1]
