# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pickle
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Merge
from keras.layers import Embedding
from keras.layers import LSTM
from TemporalMeanPooling import TemporalMeanPooling
from keras.callbacks import ModelCheckpoint
import numpy as np
from loadData import loadData


maxlen = 100
vocabSize = 10000

trainX, trainY, testX, testY = loadData(maxlen,vocabSize)

model = Sequential()
model.add(Embedding(vocabSize, 128, input_length=maxlen,mask_zero=True))
model.add(LSTM(output_dim=128, activation='tanh',
               inner_activation='hard_sigmoid',
               return_sequences=True))
model.add(TemporalMeanPooling())
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adadelta',
              metrics=['accuracy'])
model.fit(trainX, trainY, batch_size=16, nb_epoch=30,
          validation_data = (testX[:500],testY[:500]),
          callbacks=[ModelCheckpoint('models/one-lstm/{epoch:02d}-{val_acc:.2f}.hdf5',
                                     monitor='val_acc',save_best_only=True)])
