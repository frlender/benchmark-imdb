from keras.models import load_model
from TemporalMeanPooling import TemporalMeanPooling
from GroupedMeanPooling import GroupedMeanPooling
from loadData import loadCustom, getSaveDir

#path = 'data/sentMax-20-wordMax-30-imdb.pkl'
path = 'data/sentMax-12-wordMax-35-maxlen-100-imdb.pkl'
trainX, trainY, validX, validY, testX, testY, sentMax, wordMax, vocabSize \
= loadCustom(path)
saveDir = 'models/sentMax-12-wordMax-35-maxlen-100-imdb'


model = load_model(saveDir+'/08-0.86.hdf5',
                   custom_objects={
                   'TemporalMeanPooling':TemporalMeanPooling,
                   'GroupedMeanPooling':GroupedMeanPooling})
score = model.evaluate(testX,testY, batch_size=20)
print(model.metrics_names,score)
