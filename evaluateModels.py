#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 11:11:42 2016

@author: qiaonan
"""

from keras.models import load_model
from TemporalMeanPooling import TemporalMeanPooling
from loadData import loadData

maxlen = 100
vocabSize = 10000
_, _, testX, testY = loadData(maxlen,vocabSize)

#%%
model = load_model('models/one-lstm/04-0.82.hdf5',
                   custom_objects={'TemporalMeanPooling':TemporalMeanPooling})
score = model.evaluate(testX[500:],testY[500:], batch_size=16)
print(score)

#%%
model = load_model('models/multi-lstm/04-0.84.hdf5',
                   custom_objects={'TemporalMeanPooling':TemporalMeanPooling})
score = model.evaluate(testX[500:],testY[500:], batch_size=16)
print(score)
