#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 12:56:11 2016

@author: qiaonan
"""

from keras.models import Sequential
from keras.layers import AveragePooling1D, Masking
from TemporalMeanPooling import TemporalMeanPooling


model = Sequential([AveragePooling1D(pool_length=4,input_shape=(4,2))])
x = [[[1,2],[3,4],[5,6],[5,7]],[[1,2],[3,4],[5,6],[5,7]]]
y = model.predict(x)

model2 = Sequential()
model2.add(Masking(mask_value=0, input_shape=(3, 2)))
model2.add(TemporalMeanPooling())
x = [[[1,2],[3,4],[0,0]],[[0,0],[3,4],[7,8]]]
y = model2.predict(x)