#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 11:59:46 2016

@author: qiaonan
"""

from keras.engine.topology import Layer, InputSpec
from keras import backend as T

class GroupedMeanPooling(Layer):
    """
    This is a custom Keras layer. This pooling layer accepts the temporal
    sequence output by a recurrent layer and performs temporal pooling by
    group. In each group, it averages only the non-masked portion of the
    sequence and converts the sequence of hidden vectors into a single
    averaged vector. The group_len parameter determines the chunk size
    by which the input sequence will be divided. The output is supposed to
    be fed into another recurrent layer for more abstract representation.

    input shape: (nb_samples, nb_timesteps, nb_features)
    output shape: (nb_samples, nb_groups, nb_features)
    """
    def __init__(self, group_len, **kwargs):
        super(GroupedMeanPooling, self).__init__(**kwargs)
        self.group_len = group_len
        self.supports_masking = True
        self.input_spec = [InputSpec(ndim=3)]

    def get_output_shape_for(self, input_shape):
        group_count = int(input_shape[1]/self.group_len)
        return (input_shape[0], group_count, input_shape[2])

    def call(self, x, mask=None): #mask: (nb_samples, nb_timesteps)
        if mask is None:
            mask = T.mean(T.ones_like(x), axis=-1)

        if hasattr(x, '_keras_shape'):
            input_shape = x._keras_shape
        elif hasattr(T, 'int_shape'):
            input_shape = T.int_shape(x)

        timestep_size = input_shape[1]
        feature_size = input_shape[2]
        group_len = self.group_len
        group_count = int(timestep_size/group_len)

        mask = T.cast(mask,T.floatx())
        xr = T.reshape(x,(-1,group_count,group_len,feature_size))
        maskr = T.reshape(mask,(-1,group_count,group_len))

        ssum = T.sum(xr,axis=-2) #(nb_samples, nb_groups, np_features)

        rcnt = T.sum(maskr,axis=-1,keepdims=True) #(nb_samples, nb_groups)
        # avoid zero division
        rcnt_mask = T.lesser(rcnt,0.5)
        rcnt += T.cast(rcnt_mask,T.floatx())

        return ssum/rcnt

    def compute_mask(self, input, mask):
        if hasattr(mask, '_keras_shape'):
            mask_shape = mask._keras_shape
        elif hasattr(T, 'int_shape'):
            mask_shape = T.int_shape(mask)
        group_len = self.group_len
        group_count = int(mask_shape[1]/group_len)

        maskr = T.reshape(mask,(-1,group_count,group_len))
        return T.any(maskr,axis=-1,keepdims=False)

    def get_config(self):
        config = {'group_len': self.group_len}
        base_config = super(GroupedMeanPooling, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
