from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from TemporalMeanPooling import TemporalMeanPooling
from GroupedMeanPooling import GroupedMeanPooling
from keras.callbacks import ModelCheckpoint
from loadData import loadCustom,getSaveDir

# path = 'data/sentMax-20-wordMax-30-imdb.pkl'
path = 'data/sentMax-12-wordMax-35-maxlen-100-imdb.pkl'
trainX, trainY, validX, validY, testX, testY, sentMax, wordMax, vocabSize \
= loadCustom(path)
saveDir = getSaveDir(path)

emb_dim = 200


model = Sequential()
model.add(Embedding(vocabSize,emb_dim,input_length=wordMax*sentMax,mask_zero=True))
model.add(LSTM(output_dim=emb_dim, activation='tanh',
               inner_activation='hard_sigmoid',
               return_sequences=True))
model.add(GroupedMeanPooling(group_len=wordMax))
model.add(LSTM(output_dim=emb_dim, activation='tanh',
               inner_activation='hard_sigmoid',
               return_sequences=True))
model.add(TemporalMeanPooling())
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adadelta',
              metrics=['accuracy'])
model.fit(trainX, trainY, batch_size=20, nb_epoch=10,
          validation_data = (validX,validY),
          callbacks=[ModelCheckpoint(
          saveDir+'/{epoch:02d}-{val_acc:.2f}.hdf5',
          monitor='val_acc',save_best_only=True)])
