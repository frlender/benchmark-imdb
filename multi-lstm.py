# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from TemporalMeanPooling import TemporalMeanPooling
from keras.callbacks import ModelCheckpoint
from loadData import loadData


maxlen = 100
vocabSize = 10000
proj_dim = 64

trainX, trainY, testX, testY = loadData(maxlen,vocabSize)

model = Sequential()
model.add(Embedding(vocabSize, proj_dim, input_length=maxlen,mask_zero=True))
model.add(LSTM(output_dim=proj_dim, activation='tanh',
               inner_activation='hard_sigmoid',
               return_sequences=True))
model.add(LSTM(output_dim=proj_dim, activation='tanh',
               inner_activation='hard_sigmoid',
               return_sequences=True))
model.add(LSTM(output_dim=proj_dim, activation='tanh',
               inner_activation='hard_sigmoid',
               return_sequences=True))
model.add(TemporalMeanPooling())
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adadelta',
              metrics=['accuracy'])
model.fit(trainX, trainY, batch_size=16, nb_epoch=10,
          validation_data = (testX[:500],testY[:500]),
          callbacks=[ModelCheckpoint('models/multi-lstm/{epoch:02d}-{val_acc:.2f}-64dim-3layers.hdf5',
                                     monitor='val_acc',save_best_only=True)])
