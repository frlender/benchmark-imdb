from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation,Masking
from keras.layers import Embedding
from keras.layers import LSTM
from TemporalMeanPooling import TemporalMeanPooling
from GroupedMeanPooling import GroupedMeanPooling
from keras.callbacks import ModelCheckpoint
from loadData import loadData
import numpy as np

model = Sequential()
model.add(Masking(mask_value=0., input_shape=(6, 2)))
model.add(GroupedMeanPooling(group_len=2))
model.add(TemporalMeanPooling())
model.compile(loss='mean_squared_error',optimizer='adadelta',
batch_size=1)
input = np.array([[[1,2],[3,4],[5,6],[0,0],[0,0],[0,0]],
[[1,2],[3,4],[0,0],[0,0],[0,0],[0,0]]])
print(model.predict(input))
